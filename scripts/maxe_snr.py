from snrcalc import (cli,
                     logger,
                     )
import matplotlib.pyplot as plt
import snrcalc.snr as main


def tabulate_snr(args):
    logger.info('observe target {} [{}mag]'.format(
                    args.config['signal']['target'],
                    args.config['telescope']['filter']))
    logger.info('observed sky {} [{}mag/arcsec^2]'.format(
                    args.config['signal']['sky'],
                    args.config['telescope']['filter']))

    logger.info('Noise sources and quantisation')
    logger.info('\tQuantisation efficiency: Qe = {}'.format(
        args.config['ccd']['qe']))
    logger.info('\tDark current: dc = {:.6f} [e/p/s]'.format(
        args.config['ccd']['dark']))
    logger.info('\tCharge tranfser efficiency: CTE = {:.5f}'.format(
        args.config['ccd']['cte']))
    logger.info('\tReadout noise: R = {} [e/p]'.format(
        args.config['ccd']['readout']))
    logger.info('\tGain: g = {} [e/ADU]'.format(
        args.config['instrument']['gain']))

    logger.info('Fibre to pixel mapping')
    logger.info('\tTotal number fibres {}'.format(
        args.config['instrument']['nifu']))
    logger.info('\tv-binned {} (# fibres)'.format(
        args.config['binning']['vbin']))
    logger.info('\tfibre fill factor at slit {:.3f}'.format(
        args.config['instrument']['fv']))
    logger.info('\t# fibre entrance pixels {}'.format(
        args.config['instrument']['nc']))
    logger.info('\t# binned pixels {}'.format(
        args.config['binning']['nbp']))
    logger.info('\t# total pixels {}'.format(
        args.config['instrument']['np']))

    [s, n, snr] = main.get_snr(args.config)
    logger.info('SNR calculation')
    logger.info('   t\t\tS\t\tN\t\tS/N')
    for idx, t in enumerate(args.config['signal']['time']):
        logger.info('{:4d}\t{:11.6f}\t{:11.6f}\t{:11.6f}'.format(
            int(t), s[idx], n[idx], snr[idx]))


def ds_range_plot(args):
    # fibre core separation range
    snr_range = []
    fill_range = []
    for idx, ds in enumerate(args.ds_range):
        args.config['instrument']['ds'] = ds
        args.config['instrument']['fv'] = args.config['instrument']['dc'] / ds
        nppf = int(args.config['instrument']['nc'] /
                   args.config['instrument']['fv']) + 1
        args.config['instrument']['np'] = nppf * \
            args.config['instrument']['nifu']

        fill_range.append(args.config['instrument']['fv'])
        s, n, snr = main.get_snr(args.config)
        snr_range.append(snr)

    low_separation_snr = snr_range[0][:]
    avg_separation_snr = snr_range[1][:]
    high_separation_snr = snr_range[2][:]

    fig = plt.figure(num=1,
                     figsize=(17, 8),
                     facecolor='w',
                     edgecolor='k')

    plt.loglog(args.config['signal']['time'],
               avg_separation_snr,
               '.-',
               color='k',
               label='average fill factor {:.3f}'.format(
                   fill_range[1]))
    plt.fill_between(args.config['signal']['time'],
                     avg_separation_snr,
                     low_separation_snr,
                     color='grey',
                     alpha='0.5')
    plt.fill_between(args.config['signal']['time'],
                     avg_separation_snr,
                     high_separation_snr,
                     color='grey',
                     alpha='0.5')

    plt.axhline(y=5, color='b', linestyle='--')
    plt.grid(True)
    plt.legend(loc=0)
    plt.axis('tight')
    plt.ylabel('log SNR')
    plt.xlabel('log time [sec]')
    fig.savefig('maxE_snr.png', dpi=fig.dpi)


if __name__ == '__main__':
    args = cli()
    if not args.config['signal']['time']:
        print('empty time string, nothing to do, exiting.....')
        exit()
    if args.ds_range is not None:
        ds_range_plot(args)
    else:
        tabulate_snr(args)

    if not args.silent:
        plt.show()
# -fin-
