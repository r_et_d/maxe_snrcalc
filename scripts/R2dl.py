# Helper script to calculate wavelength step size given expected resolution

from argparse import (ArgumentParser,
                      ArgumentDefaultsHelpFormatter)

import matplotlib.pyplot as plt
import numpy


# Script input and optional parameters
def cli():
    usage = " \
            \n%(prog)s [options] -R value [value ...] -l value [value ...]\
          "
    description = "wavelength step size calculator"
    parser = ArgumentParser(usage=usage,
                            description=description,
                            formatter_class=ArgumentDefaultsHelpFormatter,
                            )
    parser.add_argument('-R', '--resolution',
                        required=True,
                        nargs='*',
                        type=float,
                        help='space separated list of values defining the '
                             'range of desired resolution values')
    parser.add_argument('-l', '--wavelength',
                        required=True,
                        nargs='*',
                        type=float,
                        help='space separated list of values defining the '
                             'wavelength band [in nm units]')
    parser.add_argument('--desired',
                        type=float,
                        help='desired resolution value '
                             'to find wave step size for')

    return parser.parse_args()


def wave_steps(res,  # array of spectral resolution values
               wavel,  # array of wavelengths over the waveband
               nstep=5,
               ):
    # range of dl values: lmax/Rmax <= dl <= lmin/Rmin
    dl_min = numpy.max(wavel)/numpy.max(res)
    dl_max = numpy.min(wavel)/numpy.min(res)
    step = (dl_max - dl_min)/float(nstep)
    dl_range = numpy.arange(dl_min, dl_max+step, step)[::-1]
    dl_vals = []
    for dl in dl_range:
        dl_vals.append(wavel/dl)
    return [dl_range, numpy.asarray(dl_vals)]


def main(res,  # array of spectral resolution values
         wavel,  # array of wavelengths over the waveband
         desired_res=None,
         ):
    res = numpy.asarray(res)
    wavel = 10.*numpy.asarray(wavel)  # nm -> A
    if desired_res is not None:
        dl_range, dl_vals = wave_steps(res, wavel, nstep=1000)
    else:
        dl_range, dl_vals = wave_steps(res, wavel)

    if desired_res is not None:
        start_vals = numpy.abs(desired_res-dl_vals[:, 0])**2.
        end_vals = numpy.abs(desired_res-dl_vals[:, -1])**2.
        dl_idx = numpy.argmin(numpy.sqrt(start_vals + end_vals))
        dl_vals = dl_vals[dl_idx]
        dl_range = [dl_range[dl_idx]]

    wavel_nm = wavel/10.
    plt.figure(num=1,
               figsize=(15, 8),
               facecolor='w',
               edgecolor='k')
    if len(dl_range) < 2:
        plt.plot(wavel_nm, dl_vals,
                 '.-',
                 label=r'd$\lambda$={}'.format(dl_range[0]))
    else:
        for cnt, dls in enumerate(dl_vals):
            plt.plot(wavel_nm, dls,
                     '-.',
                     label=r'd$\lambda$={}'.format(dl_range[cnt]))
    plt.axhline(y=desired_res, color='k', linestyle='--')
    plt.fill_between(wavel_nm,
                     numpy.min(res),
                     numpy.max(res),
                     color='grey',
                     alpha='0.1')
    plt.axis('tight')
    plt.legend(loc=0)
    plt.xlabel(r'wavelength band $\lambda$ [nm]',
               fontsize=14)
    plt.ylabel(r'resolution $R=\frac{\lambda}{d\lambda}$',
               fontsize=14)
    plt.show()


if __name__ == '__main__':
    args = cli()
    main(args.resolution,
         args.wavelength,
         desired_res=args.desired,
         )

# -fin-
