from snrcalc import (cli,
                     logger,
                     )
import matplotlib.pyplot as plt
import snrcalc.snr as main


def tabulate_time(args):
    logger.info('observe target {} [{}mag]'.format(
                    args.config['signal']['target'],
                    args.config['telescope']['filter']))
    logger.info('observed sky {} [{}mag/arcsec^2]'.format(
                    args.config['signal']['sky'],
                    args.config['telescope']['filter']))

    logger.info('Noise sources and quantisation')
    logger.info('\tQuantisation efficiency: Qe = {}'.format(
        args.config['ccd']['qe']))
    logger.info('\tDark current: dc = {:.6f} [e/p/s]'.format(
        args.config['ccd']['dark']))
    logger.info('\tCharge tranfser efficiency: CTE = {:.5f}'.format(
        args.config['ccd']['cte']))
    logger.info('\tReadout noise: R = {} [e/p]'.format(
        args.config['ccd']['readout']))
    logger.info('\tGain: g = {} [e/ADU]'.format(
        args.config['instrument']['gain']))

    logger.info('Fibre to pixel mapping')
    logger.info('\tTotal number fibres {}'.format(
        args.config['instrument']['nifu']))
    logger.info('\tv-binned {} (# fibres)'.format(
        args.config['binning']['vbin']))
    logger.info('\tfibre fill factor at slit {:.3f}'.format(
        args.config['instrument']['fv']))
    logger.info('\t# fibre entrance pixels {}'.format(
        args.config['instrument']['nc']))
    logger.info('\t# binned pixels {}'.format(
        args.config['binning']['nbp']))
    logger.info('\t# total pixels {}'.format(
        args.config['instrument']['np']))

    time = main.get_time(args.config)
    logger.info('Integration time calculation')
    logger.info('S/N\t\tt')
    for idx, t in enumerate(args.config['signal']['snr']):
        logger.info('{}\t{:11.6f}'.format(
            t, time[idx]))


def ds_range_plot(args):
    # fibre core separation range
    time_range = []
    fill_range = []
    for idx, ds in enumerate(args.ds_range):
        args.config['instrument']['ds'] = ds
        args.config['instrument']['fv'] = args.config['instrument']['dc'] / ds
        nppf = int(args.config['instrument']['nc'] /
                   args.config['instrument']['fv']) + 1
        args.config['instrument']['np'] = nppf * \
            args.config['instrument']['nifu']

        fill_range.append(args.config['instrument']['fv'])
        time = main.get_time(args.config)
        time_range.append(time)

    low_separation_time = time_range[0][:]
    avg_separation_time = time_range[1][:]
    high_separation_time = time_range[2][:]

    fig = plt.figure(num=1,
                     figsize=(17, 8),
                     facecolor='w',
                     edgecolor='k')

    plt.loglog(args.config['signal']['snr'],
               avg_separation_time,
               '.-',
               color='k',
               label='average fill factor {:.3f}'.format(
                   fill_range[1]))
    plt.fill_between(args.config['signal']['snr'],
                     avg_separation_time,
                     low_separation_time,
                     color='grey',
                     alpha='0.5')
    plt.fill_between(args.config['signal']['snr'],
                     avg_separation_time,
                     high_separation_time,
                     color='grey',
                     alpha='0.5')

    plt.axhline(y=1800, color='b', linestyle='--')
    plt.axvline(x=10, color='b', linestyle='--')
    plt.grid(True)
    plt.legend(loc=0)
    plt.axis('tight')
    plt.ylabel('log time [sec]')
    plt.xlabel('log SNR')
    fig.savefig('maxE_time.png', dpi=fig.dpi)


if __name__ == '__main__':
    args = cli()
    if not args.config['signal']['snr']:
        print('empty snr string, nothing to do, exiting.....')
        exit()
    if args.ds_range is not None:
        ds_range_plot(args)
    else:
        tabulate_time(args)

    if not args.silent:
        plt.show()
# -fin-
