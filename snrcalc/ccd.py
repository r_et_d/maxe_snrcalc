class CCD231(object):
    def __init__(self,
                 config,
                 ):

        # defaults from datasheet
        self.Px = 15.  # microm
        self.Py = 15.  # microm
        self.qe = 0.9  # quantisation efficiency
        self.cte = 0.99999  # charge transfer efficiency
        self.dark = 0.000152778  # dark current [e/p/s]
        self.read = 2.8  # readout noise [e/p]

        # updates from config
        # pixel size assuming square pixels [mu.m]
        if 'pixel' in config:
            self.Px = config['pixel']
            self.Py = config['pixel']
        # efficiency to quantise photons to electrons
        # quatisation efficiency 0<=Q<=1
        if 'qe' in config:
            self.qe = config['qe']
        # charge transfer efficiency 0<=e<=1
        if 'cte' in config:
            self.cte = config['cte']
        # time independent readout noise [e/p]
        if 'readout' in config:
            self.read = config['readout']
        # thermal dark current [e/p/s]
        # in data sheet give as [e/p/hr]
        if 'dark' in config:
            self.dark = config['dark']

# -fin-
