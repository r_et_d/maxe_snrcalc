import utility


class SALT(object):
    def __init__(self):
        self.diameter = 1000  # cm
        self.obscuration = 400  # cm
        # collection area of SALT mirror
        self.mirror = utility.circle_area(diameter=self.diameter,
                                          obscuration=self.obscuration)

# -fin-
