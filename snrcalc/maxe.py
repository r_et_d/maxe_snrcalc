import numpy
import snrcalc
import utility

from telescopes import SALT

logger = snrcalc.logger


class MaxE(object):
    def __init__(self,
                 qe,  # quantisation efficiency
                 cte,  # charge transfer efficiency
                 dark,  # dark current [e/p/s]
                 read,  # readout noise [e/p]
                 np,  # total number pixels
                 nbp,  # number of pixel in a bin
                 gain=0.,  # gain
                 ):

        # self.mirror = SALT().area
        self.mirror = SALT.area
        logger.debug('SALT mirror area {:2e} [cm^2]'.format(
            self.mirror))
        self.efficiency = 0.3
        logger.debug('Instrument efficiency {}'.format(
            self.efficiency))

        self.qe = qe
        self.cte_efficiency = cte**np
        self.np = np

        # dark current [e/s]
        self.Delta_Sd = dark * np
        # readout noise [e]
        self.Sr = (read**2 + (gain/2.)**2) * (np / nbp)

        # TODO: check this divide by 3
        # number samples per resolution element
        self.samples_prs = 3.
        logger.debug('# samples per resolution element {}'.format(
            self.samples_prs))

    def photon_flux(self, observed_flux):
        # How many photons are incident per second on the mirror
        flux = self.mirror * observed_flux  # ph/s/A (theoretical)
        # photon flux per resolution element
        flux *= self.dlambda  # ph/s
        # efficiency of instrument to collect photons
        flux *= self.efficiency  # ph/s (actual)
        return flux

    def signal(self,
               target,
               sky=0.,
               R=800,
               L=5700,  # [A]
               fov=2.5,  # [arcsec]
               ):

        # ability of the instrument to separate adjacent lines
        self.dlambda = utility.spectral_resolution(R=R,  # resolution
                                                   L=L)  # wavelength [A]
        logger.debug('Spectral resolution {}'.format(
            self.dlambda))

        # point source flux [ph/s]
        self.target = self.photon_flux(target)
        logger.debug('flux per resolution element {:.3} [ph/s]'.format(
            self.target))
        self.target /= self.samples_prs  # ph/s
        logger.debug('flux per resolution sample {:.3} [ph/s]'.format(
            self.target))

        # sky contribution is distributed across the field of view
        self.sky = self.photon_flux(sky)
        self.fov = fov  # [arcsec]
        logger.debug('fibre entrance field of view {} [arcsec^2]'.format(
            self.fov))
        # FOV is diameter
        self.sky *= numpy.pi*(self.fov/2.)**2.
        logger.debug('sky flux per resolution element '
                     'over FOV {:.3} [ph/s]'.format(
                         self.sky))
        self.sky /= self.samples_prs  # ph/s
        logger.debug('sky flux per resolution sample {:.3} [ph/s]'.format(
            self.sky))

    def snr(self,
            dt,  # integration time [sec]
            ):
        # convert photons collected to nr electrons collected
        # Signal = (f* x Qe x dt)
        # electrons detected from the source
        dt = numpy.array(dt)
        self.S = (self.target * self.qe * self.cte_efficiency) * dt
        logger.debug('Signal {} [e]'.format(
            self.S))

        # background noise [e/p]
        self.Sb = (self.target + self.sky) * self.qe * dt
        # dark current [e/p]
        self.Sd = self.Delta_Sd * dt

        # total noise
        self.N = numpy.sqrt(self.Sb + self.Sd + self.Sr)
        logger.debug('Noise = sqrt( {} + {} + {}) = {} [e]'.format(
            self.Sb, self.Sd, self.Sr, self.N))
        logger.debug('SNR = {}'.format(
            self.S/self.N))

        SNR = self.S/self.N

        return [SNR.tolist(),
                self.S.tolist(),
                self.N.tolist()]

    def time(self,
             snr,  # desired snr
             ):
        snr = numpy.array(snr)
        # signal [e/p/s]
        self.S = self.target * self.qe * self.cte_efficiency
        # background noise [e/p/s]
        self.Sb = (self.target + self.sky) * self.qe
        # dark current [e/p/s]
        self.Sd = self.Delta_Sd
        # total noise
        self.N = numpy.sqrt(self.Sb + self.Sd + self.Sr)

        # coefficients for solving quadratic equation
        a = 1.
        b = - (snr**2. * (self.Sb+self.Sd)) / self.S**2.
        c = - (snr**2. * self.Sr) / self.S**2.

        # int time
        int_time = (-b + numpy.sqrt(b**2. - 4.*a*c)) / (2.*a)

        return int_time.tolist()

# -fin-
