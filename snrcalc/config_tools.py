import ccd
import instruments
import snrcalc
import telescopes

from config_parser import config_parser

logger = snrcalc.logger


# read config file in as argument
def read_config(value):
    return config_parser(value)


# update config values if specified by user
def update_config(parser, section, arg_list):
    for arg in arg_list:
        if parser.__dict__[arg] is not None:
            if section not in parser.config:
                parser.config[section] = {}
            if arg not in parser.config[section]:
                parser.config[section][arg] = None
            logger.debug('Update {}:{} from {} to {}'.format(
                section,
                arg,
                parser.config[section][arg],
                parser.__dict__[arg]))
            parser.config[section][arg] = parser.__dict__[arg]
        # remove update arg from namespace
        # to prevent confusion with config
        delattr(parser, arg)
    return parser


def config_components(config):
    target_ = config['signal']['target']
    sky_ = config['signal']['sky']
    filter_ = config['telescope']['filter']

    name_ = str.lower(config['telescope']['name'])
    if name_ == 'salt':
        telescope_ = telescopes.SALT
        logger.debug('SALT mirror area {:2e} [cm^2]'.format(
            telescope_.mirror))

    name_ = str.lower(config['ccd']['name'])
    if name_ == 'ccd231':
        ccd_ = ccd.CCD231(config['ccd'])

    binning_ = config['binning']

    name_ = str.lower(config['instrument']['name'])
    if name_ == 'maxe':
        instrument_ = instruments.MaxE(config['instrument'],
                                       ccd_,
                                       binning_)

    return [target_,
            sky_,
            filter_,
            telescope_,
            instrument_,
            ccd_,
            binning_,
            ]

# -fin-
