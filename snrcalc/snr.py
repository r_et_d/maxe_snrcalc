import instruments
from snrcalc import logger
from standards import Vega


def main(instrument_config):
    filter_ = instrument_config['telescope']['filter']
    target_mag = instrument_config['signal']['target']
    sky_mag = instrument_config['signal']['sky']

    # calculate sky flux using Vega standards
    vega = Vega(filter_)
    signal_flux = vega.flux(target_mag)  # ph/s/cm^2/A
    logger.debug('observed target flux {:.3e} [ph/s/cm^2]'.format(
                    signal_flux))
    sky_flux = vega.flux(sky_mag)  # ph/s/cm^2/A/arcsec^2
    logger.debug('observed sky flux {:.3e} [ph/s/cm^2/arcsec^2]'.format(
                    sky_flux))

    instr = instruments.MaxE(instrument_config['ccd']['qe'],
                             instrument_config['ccd']['cte'],
                             instrument_config['ccd']['dark'],
                             instrument_config['ccd']['readout'],
                             instrument_config['instrument']['np'],
                             instrument_config['binning']['nbp'],
                             gain=instrument_config['instrument']['gain'],
                             )

    R = instrument_config['instrument']['r']
    L = instrument_config['instrument']['l']
    FOV = instrument_config['instrument']['fov']
    # target and sky magnitudes to instrument fluxes
    instr.signal(target=signal_flux,
                 sky=sky_flux,
                 R=R,
                 L=L,
                 fov=FOV,
                 )
    logger.debug('target photon flux {:.2e} [ph/s]'.format(
                    instr.target))
    logger.debug('sky photon flux {:.2e} [ph/s]'.format(
                    instr.sky))
    return instr

def get_snr(instrument_config):
    instr = main(instrument_config)
    # all parameters in e/p and snr
    [snr, s, n] = instr.snr(instrument_config['signal']['time'])
    return [s, n, snr]

def get_time(instrument_config):
    instr = main(instrument_config)
    # all parameters in e/p and snr
    time = instr.time(instrument_config['signal']['snr'])
    return time

# -fin-
