import logging
import snrcalc

logger = snrcalc.logger


# Vega flux zeropoints
class Namespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


# http://www.astronomy.ohio-state.edu/~martini/usefuldata.html
class Vega(object):
    def __init__(self, filter_):
        filter_coeffs = {
                       'B': {'fnu': 4.063,  # x10-20 erg cm-2 s-1 Hz-1
                             'flambda': 632.,  # x10-11 erg cm-2 s-1 A-1
                             'philambda': 1392.6},  # photons cm-2 s-1 A-1
                       'V': {'fnu': 3.636,
                             'flambda': 363.1,
                             'philambda': 995.5},
                       'R': {'fnu': 3.064,
                             'flambda': 217.7,
                             'philambda': 702.0},
                        }
        self.coeffs = filter_coeffs[str.upper(filter_)]
        logger.debug('Coeffs for filter {}, '
                     '(fnu={}, flambda={}, philambda={})'.format(
                         filter_,
                         self.coeffs['fnu'],
                         self.coeffs['flambda'],
                         self.coeffs['philambda']))
        self.coeffs = Namespace(fnu=self.coeffs['fnu'],
                                flambda=self.coeffs['flambda'],
                                philambda=self.coeffs['philambda'])

    # convert magnitude to photon flux
    def flux(self, target_mag):
        vega_flux = self.coeffs.philambda  # ph/s/cm^2/A
        # Star flux relative to Vega
        # F* = Fvega x 10 ^ (-0.4m*)
        return (vega_flux * (10.**(-target_mag/2.5)))


if __name__ == '__main__':
    logger.setLevel(logging.DEBUG)

    filters = ['b', 'R', 'V']
    for filter_ in filters:
        vega = Vega(filter_)
        logger.debug('Vega filter {}, coeffs {}, flux {} = 0. mag'.format(
            filter_,
            vega.coeffs.__dict__,
            vega.flux(0.)))

# -fin-
