__version__ = '0.1a'

import logging
import sys


class SpecialFormatter(logging.Formatter):
    debugFORMAT = "[ %(levelname)s - %(asctime)s - %(filename)s:%(lineno)s] %(message)s"
    FORMATS = {logging.DEBUG: debugFORMAT,
               logging.ERROR: "ERROR: %(message)s",
               logging.INFO: "%(message)s",
               'DEFAULT': "%(levelname)s: %(message)s"}

    def format(self, record):
        self._fmt = self.FORMATS.get(record.levelno, self.FORMATS['DEFAULT'])
        self.datefmt = "%Y-%m-%d %H:%M:%S"
        return logging.Formatter.format(self, record)


logger = logging.getLogger("snrcalc")
# create and add console handler
hdlr = logging.StreamHandler(sys.stdout)
hdlr.setFormatter(SpecialFormatter())
logging.root.addHandler(hdlr)
logging.root.setLevel(logging.INFO)
# create and add logfile handler
f_handler = logging.FileHandler('/tmp/snrcalc.log')
f_handler.setFormatter(SpecialFormatter())
logger.addHandler(f_handler)


import flux  # noqa
import instruments  # noqa
import snr  # noqa
import standards  # noqa
import telescopes  # noqa
from __main__ import cli  # noqa
from config_tools import config_components  # noqa

# -fin-
