import numpy


def circle_area(diameter,
                obscuration=0,
                ):
    # area of a circle: A = PI*R^2
    def area(radius):
        return numpy.pi*(radius**2)
    total = area(diameter/2)
    obstructed = area(obscuration/2)
    return (total-obstructed)


# ability of the instrument to separate adjacent lines
def spectral_resolution(R=None,  # resolution
                        L=None,  # central wavelength [A]
                        dL=0.):
    if (L is not None):
        if dL > 0.:
            return (float(L)/dL)
        if R is not None:
            return (float(L)/float(R))
    else:
        return float(R)*float(dL)

# -fin-
