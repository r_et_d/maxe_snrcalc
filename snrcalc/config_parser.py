import ConfigParser
import json
import logging
import os
import snrcalc
import sys

logger = snrcalc.logger


# Read config .ini file
def read_config(filename):
    if not os.path.isfile(filename):
        raise RuntimeError('Cannot read file {}'.format(
            filename))

    input_ = {}
    config = ConfigParser.SafeConfigParser()
    config.read(filename)
    logger.debug('Config sections: {}'.format(
                 config.sections()))
    for section in config.sections():
        input_[section] = {}
        logger.debug('sections{}: {}'.format(
                     section,
                     config.options(section)))
        for option in config.options(section):
            try:
                input_[section][option] = float(config.get(section,
                                                           option))
            except ValueError:
                input_[section][option] = config.get(section,
                                                     option)
    return input_


def config_parser(filename):
    return read_config(filename)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise SystemExit('python config_parser.py <filename.ini>')
    logger.setLevel(logging.DEBUG)
    filename = sys.argv[1]
    input_ = config_parser(filename)
    logger.debug('config input:\n{}'.format(
        json.dumps(input_,
                   sort_keys=True,
                   indent=4)))

# -fin-
