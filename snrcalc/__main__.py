import argparse
import logging
import snrcalc

from config_tools import read_config, update_config
logger = snrcalc.logger


def cli():
    usage = "%(prog)s [options] --config <config_file>"
    description = "CCD SNR calculations for point source targets"
    parser = argparse.ArgumentParser(usage=usage,
                                     description=description,
                                     )
    parser.add_argument('--version',
                        action='version',
                        version=snrcalc.__version__)
    parser.add_argument(
            '--config',
            type=read_config,
            required=True,
            help='signal and system configuration file, **required**')

    group = parser.add_argument_group(
           title='observed signal information',
           description='values related to astronomy target observation, '
                       'to update original config values, '
                       'or assign values if not specified in config')
    group.add_argument(
            '--target',
            type=str,
            help='magnitude of target source '
                 '[ph/s/cm^2/A], or '
                 'space separated file tabulating fl spectrum '
                 '[ph/s/A]')
    group.add_argument(
            '--sky',
            type=float,
            help='magnitude of sky background contribution '
                 '[ph/s/cm^2/A/arcsec^2], or '
                 'space separated file tabulating sky spectrum '
                 '[ph/s/A/arcsec^2]')
    group.add_argument(
            '--time',
            nargs='*',
            type=float,
            help='list of exposure times, each value in seconds')

    group = parser.add_argument_group(
           title='telescope information',
           description='values related to the telescope '
                       'the instrument is mounted on')
    choices = ['B', 'V', 'R']
    group.add_argument(
            '--filter',
            type=str,
            choices=choices,
            help='select filter option, e.g. V')

    group = parser.add_argument_group(
           title='pixel binning information')
    group.add_argument(
            '--nh',
            type=int,
            help='number horizontal pixels to bin')
    group.add_argument(
            '--nv',
            type=int,
            help='number vertical pixels to bin')
    group.add_argument(
            '--hbin',
            type=int,
            help='number of bins to stack horizontally into a super pixel')
    group.add_argument(
            '--vbin',
            type=int,
            help='number of bins to stack vertically into a super pixel')

    group = parser.add_argument_group(
           title='development specific options',
           description='more verbose logging and output options '
                       'to verify functionality and debug issues')
    group.add_argument('--debug',
                       action='store_true',
                       help='show verbose output for debugging')
    group.add_argument('--silent',
                       action='store_true',
                       help='no output, run silently')

    args = parser.parse_args()

    # either a list of time for snr calculations
    # or a list of snr to estimate integration times
    for key in ['time', 'snr']:
        if key in args.config['signal'].keys():
            time_list = str(args.config['signal'][key]).split()
            args.config['signal'][key] = [float(val.strip())
                                          for val in time_list
                                          ]
        else:
            args.config['signal'][key] = None

    if args.debug:
        logger.setLevel(logging.DEBUG)
    if args.silent:
        logger.setLevel(logging.WARN)

    logger.debug('original args\n{}'.format(
        args))
    logger.debug('config args\n{}'.format(
        args.config))

    # add and update config information with input arguments
    sig_list = ['target',
                'sky',
                'time',
                ]
    args = update_config(args,
                         'signal',
                         sig_list,
                         )
    tel_list = ['filter',
                ]
    args = update_config(args,
                         'telescope',
                         tel_list,
                         )

    bin_list = ['nh',
                'nv',
                'hbin',
                'vbin',
                ]
    if 'binning' in args.config.keys():
        for key in bin_list:
            if key not in args.config['binning'].keys():
                args.config['binning'][key] = 1
        args = update_config(args,
                             'binning',
                             bin_list,
                             )

    # set sensible defaults if not given by user
    if 'target' not in args.config['signal']:
        raise RuntimeError('Flux file or magnitude required for calculation, '
                           'add --target argument')
    if args.config['signal']['target'] is None:
        raise RuntimeError('Target magnitude required for calculation, '
                           'add --target argument')
    if 'sky' not in args.config['signal']:
        args.config['signal']['sky'] = 0.
    if args.config['signal']['sky'] is None:
        args.config['signal']['sky'] = 0.

    if 'filter' not in args.config['telescope']:
        args.config['telescope']['filter'] = 'V'
    if args.config['telescope']['filter'] is None:
        args.config['telescope']['filter'] = 'V'

    if 'instrument' in args.config:
        if 'gain' not in args.config['instrument']:
            args.config['instrument']['gain'] = 0.

    return args


if __name__ == '__main__':
    logger.setLevel(logging.DEBUG)
    parser = cli()

# -fin-
