import numpy
import os.path
import scipy.constants as phys

from snrcalc import logger
from standards import Vega


def magnitude2flux(
                   magnitude,
                   filter_,
                   mirror,
                   ):
    """
    Magnitude of target source [ph/s/cm^2/A]
    """
    vega = Vega(filter_)
    # if target is specified in magnitude unites as a single value
    # calculate sky flux using Vega standards
    flux = vega.flux(magnitude)  # ph/s/cm^2/A
    logger.debug('observed target flux {:.3e} [ph/s/cm^2/A]'.format(
        flux))
    # How many photons are incident per second on the mirror
    flux *= mirror  # ph/s/A (theoretical)
    logger.debug('photon flux {:.3e} [ph/s/A]'.format(
        flux))
    return flux


def fluxspectra(
                filename,
                ):
    """
    Space separated file tabulating fl spectrum [ps/s/A]
    """
    with open(filename, 'r') as fin:
        lines = fin.readlines()
    data = []
    for line in lines:
        if line[0] == '#':
            continue  # header
        values = numpy.array(line.strip().split(), dtype=float)
        if len(data) < 1:
            data = values
        else:
            data = numpy.vstack((data, values))
    wavelength = data[:, 0]
    flux_l = data[:, 1]

    return wavelength, flux_l


def map2waveband(signal,
                 wavelength,
                 waveband,
                 ):
    if isinstance(signal, float):
        return numpy.array(len(waveband)*[signal])

    sampled_signal = []
    for lambda_ in waveband:
        lidx = numpy.argmin(numpy.abs(wavelength-lambda_))
        sampled_signal.append(signal[lidx])

    return numpy.array(sampled_signal)


def input(target,
          sky,
          telescope,
          instrument,
          filter_='V',
          ):

    waveband = instrument.waveband

    def map_fluxes(signal_flux, wavelength, waveband):
        waveband_flux = map2waveband(signal_flux,
                                     wavelength,
                                     waveband,
                                     )
        signal_cnts = flux2counts(waveband_flux,
                                  waveband,  # A
                                  )  # counts/s/A
        return {'signal_flux': signal_flux,
                'waveband_flux': waveband_flux,
                'signal_cnts': signal_cnts}

    # get target and background signals
    if isinstance(target, float):
        wavelength = instrument.L  # central wavelength [A]
        signal_flux = magnitude2flux(float(target),
                                     filter_,
                                     telescope.mirror,
                                     )  # ph/s/A
        logger.debug('observed target flux {:.3e} [ph/s/A]'.format(
            signal_flux))
    elif isinstance(target, str):
        if os.path.isfile(target):
            wavelength, signal_flux = fluxspectra(target)
        else:
            raise RuntimeError('File {} does not exit'.format(target))
    else:
        raise RuntimeError('Unknown target type')
    # efficiency of instrument to collect photons
    signal_flux *= instrument.efficiency

    if isinstance(sky, float):
        sky_flux = magnitude2flux(float(sky),
                                  filter_,
                                  telescope.mirror,
                                  )  # ph/s/A/arcsec^2
        logger.debug('observed sky flux {:.3e} [ph/s/A/arcsec^2]'.format(
            sky_flux))
        # FOV is diameter
        sky_flux *= numpy.pi*(instrument.FOV/2.)**2.
        logger.debug('sky flux per resolution element '
                     'over FOV {:.3} [ph/s/A]'.format(
                         sky_flux))
    elif isinstance(target, str):
        # TODO: add background file
        raise RuntimeError('Not done')
    else:
        raise RuntimeError('Unknown target type')
    if not isinstance(wavelength, float):
        sky_flux = numpy.array(len(wavelength)*[sky_flux])
    # efficiency of instrument to collect photons
    sky_flux *= instrument.efficiency

    target_dict = map_fluxes(signal_flux, wavelength, waveband)
    sky_dict = map_fluxes(sky_flux, wavelength, waveband)

    return [wavelength,
            waveband,
            target_dict,
            sky_dict,
            ]


def flux2counts(flux,
                wavelength,
                ):
    # Units for computation
    planck_h_ergs = phys.h * 1e7  # h=J.s => *e7 ergs.s
    light_c_cm = phys.c * 100  # c=m/s => *100 cm/s^
    hc = planck_h_ergs * light_c_cm * 1e8  # ergs.A
    energy = hc / numpy.asarray(wavelength)  # ergs
    return numpy.asarray(flux)/energy  # counts/s/A


def counts2flux(counts,
                wavelength,
                ):
    # Units for computation
    planck_h_ergs = phys.h * 1e7  # h=J.s => *e7 ergs.s
    light_c_cm = phys.c * 100  # c=m/s => *100 cm/s^
    hc = planck_h_ergs * light_c_cm * 1e8  # ergs.A
    energy = hc / numpy.asarray(wavelength)  # ergs
    return numpy.asarray(counts)*energy  # counts/s/A

# -fin-
