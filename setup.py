from setuptools import setup
import glob

setup(name='snrcalc',
      description='SNR for optical spectrometers',
      long_description='Calculate expected SNR for fibre fed optical '
                       'spectrometers given user defined instrument setup '
                       'and desired source magnitudes.',
      license='GPL',
      author='Ruby van Rooyen',
      author_email='ruby at ska.ac.za',
      classifiers=[
                   'Development Status :: 2 - Alpha',
                   'Intended Audience :: Scientists',
                   'Operating System :: OS Independent',
                   'License :: OSI Approved :: GNU General Public License (GPL)',
                   'Topic :: Scientific/Engineering',
                   'Topic :: Physics :: Astronomy :: Python',
                  ],
      requires=['pylab', 'matplotlib', 'ConfigParser', 'numpy', 'scipy'],
      provides=['snrcalc'],
      packages=['snrcalc'],
      scripts=glob.glob('scripts/*.py'),
      )

# -fin-
